--/////////////////////////////////////////////////////////
--SQL Server
--/////////////////////////////////////////////////////////

--drop table DBMSMetadata_Prueba_FK;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_FK
(
  nu_cliente     tinyint      NOT NULL,
  nu_telefono    int          NULL,

  constraint PK01_DBMSMetadata_Prueba_FK
    primary key(nu_cliente),
  constraint FK01_DBMSMetadata_Prueba_FK
    foreign key(nu_cliente)
    references DBMSMetadata_Prueba_Cliente(nu_cliente),
  constraint UK01_DBMSMetadata_Prueba_FK
    unique(nu_telefono)
)


--drop table DBMSMetadata_Prueba_Cliente;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_Cliente
(

  nu_cliente             tinyint                 NOT NULL,
  co_registro            smallint                NULL,
  nu_telefono            int                     NULL,
  co_NIF                 bigint                  NOT NULL,
  qt_peso                float                   NULL,
  qt_dinero_cuenta       real                    NULL,
  qt_total_facturado     decimal                 NULL,
  pc_total_facturado     numeric(5,2)            NULL,
  nu_identification      bit                     NOT NULL,
  qt_balance             money                   NULL,
  qt_balance_menor       smallmoney              NULL,
  dt_primera_operacion   datetime                NULL,
  dt_primera_aparicion   smalldatetime           NULL,
--  dt_fecha_validez       timestamp               NOT NULL,
  co_iniciales           char(3)                 NOT NULL,
  no_nombre              varchar(10)             NULL,
  co_nombre              binary(4)               NULL,
  co_nombre_completo     varbinary(4)            NULL,
  ds_foto                image                   NULL,
  ds_text                text                    NULL,

   constraint PK01_DBMSMetadata_Prueba
     primary key(nu_cliente)
)

CREATE INDEX IX01_DBMSMetadata_Prueba
      ON DBMSMetadata_Prueba_Cliente
         (nu_telefono)

insert into DBMSMetadata_Prueba_Cliente
  select 1, 33, 91772, 50824234, 85.64, 305.50, 2500.67, 25.89, 1, 100.20, 3000.45,  cast('2012-01-01' as datetime),
  cast('2012-01-01' as smalldatetime), 'VMP', 'V�ctor', 1101, 1111, '2312', ' dwdwdfen iojef correcto '


select *
from DBMSMetadata_Prueba_Cliente



