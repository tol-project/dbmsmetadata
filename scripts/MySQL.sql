-- MySQL

-- 1) Dada una tabla listar sus columnas
DESCRIBE DBMSMetadata_Prueba;

select
  column_name,
  data_type,
  character_maximum_length,
  numeric_precision,
  numeric_scale,
  is_nullable
from INFORMATION_SCHEMA.columns
where table_name like 'DBMSMetadata_Prueba_%'

-- 2)
select TABLE_NAME
from INFORMATION_SCHEMA.tables
where table_type = 'BASE TABLE'
  and table_name like '%DBMSMetadata_Prueba%'


-- 3) Mismo que el dos mostrando el data_length
--data_length
select table_name, data_length
from INFORMATION_SCHEMA.tables
where table_name like '%fue_f_fuerzas%'

-- 4)Constraints

select
  TABLE_NAME,
  CONSTRAINT_NAME,
  CONSTRAINT_TYPE
from INFORMATION_SCHEMA.table_constraints
where table_name like '%DBMSMetadata_Prueba%'


select *
from INFORMATION_SCHEMA.KEY_COLUMN_USAGE
where table_name like '%DBMSMetadata_Prueba%'

-- 5)�ndices --MySQL crea indices para los unique

SELECT
  TABLE_NAME,
  INDEX_NAME
FROM INFORMATION_SCHEMA.STATISTICS
WHERE table_name like '%DBMSMetadata_Prueba%'

-- 6)Versi�n de MySQL

select VERSION();
