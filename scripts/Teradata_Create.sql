--/////////////////////////////////////////////////////////
-- Teradata
--/////////////////////////////////////////////////////////

--drop table DBMSMetadata_Prueba_FK;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_FK
(
  co_registro            smallint                NOT NULL,
  nu_telefono            int                     NOT NULL,

    constraint PK01_DBMSMetadata_Prueba_FK
      primary key(co_registro),
    constraint FK01_DBMSMetadata_Prueba_FK
      foreign key(co_registro)
      references DBMSMetadata_Prueba_Cliente(co_registro),
    constraint UK01_DBMSMetadata_Prueba_FK
      unique(nu_telefono)
)

--drop table DBMSMetadata_Prueba_Cliente;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_Cliente
(
  co_registro            smallint                NOT NULL,
  nu_telefono            int                     NULL,
  co_NIF                 bigint                  NOT NULL,
  qt_peso                float                   NULL,
  qt_altura              double precision        NULL,
  qt_dinero_cuenta       real                    NULL,
  qt_total_facturado     decimal                 NULL,
  pc_total_facturado     numeric(5,2)            NULL,
  dt_apertura_cuenta     date                    NOT NULL,
  dt_fecha_validez       timestamp               NOT NULL,
  ts_tiempo_operacion    time                    NULL,
  co_iniciales           char(3)                 NOT NULL,
  no_nombre              varchar(10)             NULL,
  co_nombre              byte                    NULL,
  co_nombre_completo     varbyte(4)              NULL,
  ds_blob                blob                    NULL,

  constraint PK01_DBMSMetadata_Prueba_C
    primary key(co_registro)
)

CREATE INDEX IX01_DBMSMetadata_Prueba
      ON DBMSMetadata_Prueba_Cliente
         (nu_telefono)

insert into DBMSMetadata_Prueba
select cast(0 as smallint),'aaaaa','aa', cast('2012-01-01' as date)  , 0.12, 1, 10, 100, 1

select *
from DBMSMetadata_Prueba

