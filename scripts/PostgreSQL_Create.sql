--////////////////////////////////////////////////////////////////////////////////
--PostgrSQL
--////////////////////////////////////////////////////////////////////////////////

--drop table DBMSMetadata_Prueba_FK;
--////////////////////////////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_FK
(
  co_registro    smallint      NOT NULL,
  co_char        char(5)       NOT NULL,

  constraint PK01_DBMSMetadata_Prueba_FK
    primary key(co_registro),
  constraint FK01_DBMSMetadata_Prueba_FK
    foreign key(co_registro)
    references DBMSMetadata_Prueba_Cliente(co_registro),
  constraint UK01_DBMSMetadata_Prueba_FK
    unique(co_char)
)

--////////////////////////////////////////////////////////////////////////////////
--drop table DBMSMetadata_Prueba_Cliente;
--////////////////////////////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_Cliente
(
  co_registro            smallint                NULL,
  nu_telefono            int                     NULL,
  co_NIF                 bigint                  NOT NULL,
  qt_peso                float                   NULL, --
  qt_altura              double precision        NULL,
  qt_dinero_cuenta       real                    NULL,
  qt_total_facturado     decimal                 NULL,
  pc_total_facturado     numeric(5,2)            NULL,
  nu_identification      bit                     NOT NULL,
  in_boolean             boolean                 NULL,
  qt_balance             money                   NULL,
  nu_serial              serial                  NOT NULL,
  dt_apertura_cuenta     date                    NOT NULL,
  dt_fecha_validez       timestamp               NOT NULL,
  ts_tiempo_operacion    time                    NULL,
  co_iniciales           char(3)                 NOT NULL,
  no_nombre              varchar(10)             NULL,
  co_nombre_completo     bytea                   NULL,
  ds_text                text                    NULL,

   constraint PK01_DBMSMetadata_Prueba_Cliente
     primary key(co_registro)
)

CREATE INDEX IX01_DBMSMetadata_Prueba
      ON DBMSMetadata_Prueba_Cliente
         (nu_telefono)

insert into DBMSMetadata_Prueba_Cliente
select
  2, 917721, 508123, 56.56, 1.89, 200.10, 200.10, 50.50, '1', true, '600.70', 1, date '2011-12-01', cast('2011-12-01' as timestamp),
  cast('10:25:12' as time), 'VMP', 'Victor', '112334a', 'alta realizada correctamente'

select *
from DBMSMetadata_Prueba_Cliente
