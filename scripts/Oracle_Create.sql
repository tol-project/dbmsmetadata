--/////////////////////////////////////////////////////////
--Oracle
--/////////////////////////////////////////////////////////

--drop table DBMSMetadata_Prueba_FK;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_FK
(
  co_registro            smallint     NULL,
  nu_telefono            int          NULL,

  constraint PK01_DBMSMetadata_Prueba_FK
    primary key(co_registro),
  constraint FK01_DBMSMetadata_Prueba_FK
    foreign key(co_registro)
    references DBMSMetadata_Prueba_Cliente(co_registro),
  constraint UK01_DBMSMetadata_Prueba_FK
    unique(nu_telefono)
)


--drop table DBMSMetadata_Prueba_Cliente;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_Cliente
(

  co_registro            smallint                NULL,
  nu_telefono            int                     NULL,
  qt_peso                float                   NULL,
  qt_altura              double precision        NULL,
  qt_dinero_cuenta       real                    NULL,
  qt_total_facturado     decimal                 NULL,
  pc_total_facturado     numeric(5,2)            NULL,
  pc_total               number                  NULL,
  dt_apertura_cuenta     date                    NOT NULL,
  dt_fecha_validez       timestamp               NOT NULL,
  co_iniciales           char(3)                 NOT NULL,
  no_nombre              varchar(10)             NULL,
  ds_blob                blob                    NULL,
  ds_text                long                    NULL,

    constraint PK01_DBMSMetadata_Prueba
      primary key(co_registro)
)

CREATE INDEX IX01_DBMSMetadata_Prueba
      ON DBMSMetadata_Prueba_Cliente
         (nu_telefono)

insert into DBMSMetadata_Prueba_Cliente
select
  1,1234, 78.85, 1.89, 0.12, 1.58, 10.80, 20.50, cast('01-01-2012' as date), cast('01-01-12' as timestamp), 'VMP', 'Victor',
  '11111', 'realizado pago creectamente' from dual;

select *
from DBMSMetadata_Prueba
