--/////////////////////////////////////////////////////////
-- MySQL
--/////////////////////////////////////////////////////////


--drop table DBMSMetadata_Prueba_FK;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_FK
(
  nu_cliente     tinyint      NULL,
  co_registro    smallint     NULL,

  constraint PK01_DBMSMetadata_Prueba_FK
    primary key(nu_cliente),
  constraint FK01_DBMSMetadata_Prueba_FK
    foreign key(nu_cliente)
    references DBMSMetadata_Prueba_Cliente(nu_cliente),
  constraint UK01_DBMSMetadata_Prueba_FK
    unique(co_registro)
)
 ENGINE=InnoDB



--drop table DBMSMetadata_Prueba_Cliente;
--/////////////////////////////////////////////////////////
create table DBMSMetadata_Prueba_Cliente
(

  nu_cliente             tinyint                 NOT NULL,
  co_registro            smallint                NULL,
  nu_cuenta              mediumint               NOT NULL,
  nu_telefono            int                     NULL,
  co_NIF                 bigint                  NOT NULL,
  qt_peso                float                   NULL,
  qt_altura              double                  NULL,
  qt_dinero_cuenta       real                    NULL,
  qt_total_facturado     decimal                 NULL,
  pc_total_facturado     numeric(5,2)            NULL,
  nu_identification      bit                     NOT NULL,
  dt_apertura_cuenta     date                    NOT NULL,
  dt_primera_operacion   datetime                NULL,
  dt_fecha_validez       timestamp               NOT NULL,
  ts_tiempo_operacion    time                    NULL,
  dt_ano_nacimiento      year                    NOT NULL,
  co_iniciales           char(3)                 NOT NULL,
  no_nombre              varchar(10)             NULL,
  co_nombre              binary(4)               NULL,
  co_nombre_completo     varbinary(4)            NULL,
  ds_blob                blob                    NULL,
  ds_text                text                    NULL,
  co_tipo_pago           ENUM('1', '2', '3')     NULL,
  co_set                 SET('one','two')        NULL,
  in_boolean             boolean                 NULL,

  constraint PK01_DBMSMetadata_Prueba
    primary key(nu_cliente)
)
 ENGINE=InnoDB

CREATE INDEX IX01_DBMSMetadata_Prueba
      ON DBMSMetadata_Prueba_Cliente
         (nu_telefono)

insert into DBMSMetadata_Prueba_Cliente
select
  1, 35, 33350, 916875433, 50823465, 65.2, 1.70, 350.27, 600.50, 20.54, 0, cast('2012-01-01' as date),
  cast('2012-01-01' as datetime), cast('01012012' as date), cast('06:15:53' as time), '2012', 'VMP', 'Victor', '0011', '0101',
  '0000111100001111', 'dado de alta correctamente', '5', 'one',true

select *
from DBMSMetadata_Prueba_Cliente


