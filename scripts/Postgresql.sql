---POSTGRESQL

-- 1) Dada una tabla listar todas sus columnas
-- Postgresql
select
  column_name,
  data_type,
  character_maximum_length,
  numeric_precision,
  numeric_scale,
  is_nullable
from information_schema.columns
where upper(table_name) like upper('DBMSMetadata_Prueba_Cliente')

-- 2) Listar tablas que cumplan una condici�n (tama�o)
-- Postgresql

select table_name
from information_schema.tables
where table_type = 'BASE TABLE'
  and upper(table_name) like upper('DBMSMetadata_Prueba_Cliente')
order by 1

-- 3) Funciona el tama�o

select table_name, pg_relation_size(table_name)--tama�o en bytes
from information_schema.tables
where table_type = 'BASE TABLE'
  and upper(table_name) like upper('DBMSMetadata_Prueba_Cliente')

-- 4) Constraints

select
  table_name,
  constraint_name,
  constraint_type
from INFORMATION_SCHEMA.table_constraints
where constraint_type <> 'CHECK'
  and upper(table_name) like upper('%DBMSMetadata_Prueba%')

-- 5) Indices

select
  tablename,
  indexname
from pg_indexes
where upper(tablename) like upper('%DBMSMetadata_Prueba%')

-- 6) Versi�n

select VERSION();
