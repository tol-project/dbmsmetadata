--- Oracle

--TODAS LAS TABLAS DEL DICCIONARIO
/*
  SELECT * FROM DICTIONARY
  where upper(table_name) like '%CONS%'
*/

-- 1) Dada una tabla listar todas sus columnas
select
  column_name,
  data_type,
  data_length,
  data_precision,
  data_scale,
  nullable
from ALL_TAB_COLUMNS
where upper(table_name) like upper('dbmsmetadata_prueba%')

-- 2) Listar tablas que cumplan una condici�n (tama�o)

select TABLE_NAME
from ALL_TABLES
where upper(TABLE_NAME) like upper('%dbmsmetadata_prueba%')

-- 3) Por el momento incapaz de mostrar tama�o de la tabla

select
  segment_name,
  sum(bytes) table_size
from user_extents
where segment_type='TABLE'
and upper(segment_name) like upper('%dbmsmetadata_prueba%')
group by segment_name

-- 4) Constraints
select
  TABLE_NAME,
  CONSTRAINT_NAME,
  case
    when CONSTRAINT_TYPE = 'P' then 'PRIMARY KEY'
    when CONSTRAINT_TYPE = 'R' then 'FOREIGN KEY'
    when CONSTRAINT_TYPE = 'U' then 'UNIQUE'
    else CONSTRAINT_TYPE
  end as CONSTRAINT_TYPE
from All_CONSTRAINTS
where CONSTRAINT_TYPE <> 'C'
  and upper(TABLE_NAME) like upper('dbmsmetadata_prueba%')

-- 5) Indices

select
  TABLE_NAME,
  INDEX_NAME
from ALL_INDEXES
where TABLE_NAME like upper('dbmsmetadata_prueba%')


-- 6) Versi�n

select * from v$version
where banner like '%Oracle%';

select * from product_component_version;


