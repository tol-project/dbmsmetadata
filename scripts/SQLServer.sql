--SQLServer

-- 1) Dada una tabla listar sus columnas

select
  column_name,
  data_type,
  character_maximum_length,
  numeric_precision,
  numeric_scale,
  is_nullable
from information_schema.columns
where upper(Col.table_name) like upper('DBMSMetadata_Prueba%')

-- 2)

select TABLE_NAME
from INFORMATION_SCHEMA.tables
where table_type = 'BASE TABLE'
  and table_name like '%DBMSMetadata_Prueba%'

-- 3) Tama�o funciona
select
  sysobjects.name,
  sum(
    case
      when sysindexes.indid in (0,1) then sysindexes.dpages
      else 0
    end) * 8 as Data
from sysobjects
join sysindexes
on sysobjects.id=sysindexes.id
where xtype='U' --tabla de usuario
  and sysobjects.name like '%DBMSMetadata_Prueba%'
group by sysobjects.name
order by 2 desc

-- 4) Constraints

select
  TABLE_NAME,
  CONSTRAINT_NAME,
  CONSTRAINT_TYPE
from INFORMATION_SCHEMA.table_constraints
where table_name like '%DBMSMetadata_Prueba%'

-- 5) Indices

select
  obj.name,
  ind.name
from sys.objects obj,
  sys.indexes ind
where obj.object_id = ind.object_id
  and obj.name like '%DBMSMetadata_Prueba%'

-- 6) Versi�n

Select @@version
